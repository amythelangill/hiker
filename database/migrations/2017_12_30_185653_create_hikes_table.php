<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hikes', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('hiked_with');
            $table->string('location');
            $table->text('route');
            $table->string('weather');
            $table->string('altitudes');
            $table->integer('duration');
            $table->string('sightings_and_terrain');
            $table->string('food_and_gear');
            $table->string('how_i_felt');
            $table->string('overall_rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hikes');
    }
}
