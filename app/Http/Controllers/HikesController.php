<?php

namespace App\Http\Controllers;

use App\Hike;
use Illuminate\Http\Request;

class HikesController extends Controller
{
    public function show($id) {
        $hike = Hike::find($id);
        return view('concerts.show', ['hike' => $hike]);
    }

}
