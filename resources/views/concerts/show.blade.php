<h1>{{ $hike->date->format('F j, Y') }} {{ $hike->date->format('g:ia') }}</h1>
<h2>{{ $hike->location }}</h2>
<p>Hiked With: {{ $hike->hiked_with }}</p>
<p>Route: {{ $hike->route }}</p>
<p>Weather: {{ $hike->weather }}</p>
<p>Elevation: {{ $hike->altitudes }}</p>
<p>Duration: {{ $hike->duration / 60 }} hours</p>
<p>Terrain: {{ $hike->sightings_and_terrain }}</p>
<p>Food / Gear: {{ $hike->food_and_gear }}</p>
<p>How do I feel: {{ $hike->how_i_felt }}</p>
<p>Overall Rating: {{ $hike->overall_rating }}</p>