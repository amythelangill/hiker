<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewHikeTest extends TestCase
{
    use DatabaseMigrations;
    /** @test **/
    function user_can_view_a_hike()
    {
        $hike = App\Hike::create([
            'date' => Carbon::parse('2018-01-07 11:00:00'),
            'hiked_with' => 'Mr. Frumpy',
            'location' => 'Grand Tetons National Park',
            'route' => 'Over the river 3 mi, through woulds 312 mi, Gr\'s house.',
            'weather' => 'mostly sunny',
            'altitudes' => '34.5, 22',
            'duration' => 360,
            'sightings_and_terrain' => 'very hilly',
            'food_and_gear' => 'shoes and banana',
            'how_i_felt' => 'good',
            'overall_rating' => 'superb',
        ]);

        $this->visit('/hikes/' . $hike->id);

        $this->see('January 7, 2018');
        $this->see('11:00am');
        $this->see('Mr. Frumpy');
        $this->see('Grand Tetons National Park');
        $this->see('Over the river 3 mi, through woulds 312 mi, Gr\'s house.');
        $this->see('mostly sunny');
        $this->see('34.5, 22');
        $this->see('6 hours');
        $this->see('very hilly');
        $this->see('shoes and banana');
        $this->see('good');
        $this->see('superb');
    }
}
